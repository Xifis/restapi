const express = require('express');
const mongodb = require('mongodb');

const router = express.Router();

// Get user
router.get('/:id', async (req, res) => {
    const users = await loadUsersCollection();
    res.send(await users.findOne({
        _id: new mongodb.ObjectID(req.params.id)
    }));
})

async function loadUsersCollection() {
    var client = await mongodb.MongoClient.connect('mongodb+srv://admin:admin@cluster0.klf8u.mongodb.net/cluster0?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    return client.db('cluster0').collection('users')
}

module.exports = router;