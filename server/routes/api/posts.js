const express = require('express');
const mongodb = require('mongodb');

const router = express.Router();

// Get Posts
router.get('/', async (req, res) => {
    const posts = await loadPostsCollection();
    res.send(await posts.find({}).toArray());
})

async function loadPostsCollection() {
    var client = await mongodb.MongoClient.connect('mongodb+srv://admin:admin@cluster0.klf8u.mongodb.net/cluster0?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    return client.db('cluster0').collection('posts')
}

// Add Posts
router.post('/', async (req, res) => {
    const posts = await loadPostsCollection();
    await posts.insertOne({
        content: req.body.content,
        title: req.body.title,
        description: req.body.description,
        createdAt: new Date(),
    });
    res.status(201).send();
})

// Delete Posts
router.delete('/:id', async (req, res) => {
    const posts = await loadPostsCollection();
    await posts.deleteOne({
        _id: new mongodb.ObjectID(req.params.id)
    });
    res.status(200).send();
})

module.exports = router;