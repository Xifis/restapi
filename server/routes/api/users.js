const express = require('express');
const mongodb = require('mongodb');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
// const passport = require('passport');

const saltRounds = 10;


const router = express.Router();

async function loadUsersCollection() {
    var client = await mongodb.MongoClient.connect('mongodb+srv://admin:admin@cluster0.klf8u.mongodb.net/cluster0?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    return client.db('cluster0').collection('users')
}

// Get ALL USERS FROM USERS
router.post('/users', async (req, res) => {
    const users = await loadUsersCollection();
    res.send(await users.find({}).toArray());
})

// Get ONE USER FROM USERS
router.get('/:id', async (req, res) => {
    const users = await loadUsersCollection();
    res.send(await users.findOne({
        _id: new mongodb.ObjectID(req.params.id)
    }));
})


// Add ONE USER TO USERS
router.post('/register', async (req, res) => {
    const users = await loadUsersCollection();
    const hash = bcrypt.hashSync(req.body.password, saltRounds);
    await users.insertOne({
        email: req.body.email,
        password: hash,
        username: req.body.username,
        createdAt: new Date(),
    });
    res.status(201).send();
})

// LOGIN 
router.post('/login', async (req, res) => {
    const users = await loadUsersCollection();
    await users.findOne({
            email: req.body.email
        })
        .then(user => {
            if (user.length < 1) {
                return res.status(401).json({
                    message: 'Auth failed'
                })
            }
            bcrypt.compare(req.body.password, user.password, (err, result) => {
                if (err) {
                    return res.status(401).json({
                        message: 'Auth failed'
                    })
                }
                if (result) {
                    const token = jwt.sign({
                        email: user.email,
                        userId: user._id
                    }, 'privateKey', { // to be updated with a more secure privateKey
                        expiresIn: "1h"
                    })
                    // req.headers.authorization = 'bearer ' + token
                    return res.status(200).json({
                        message: 'Auth successfull',
                        token: token
                    })
                }
                return res.status(401).json({
                    message: 'Auth failed'
                })
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
})

// Delete ONE USER FROM USERS
router.delete('/:id', async (req, res) => {
    const users = await loadUsersCollection();
    await users.deleteOne({
        _id: new mongodb.ObjectID(req.params.id)
    });
    res.status(200).send();
})

module.exports = router;