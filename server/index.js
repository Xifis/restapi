const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
// const jwt = require('jsonwebtoken');

const app = express();

// Middleware
app.use(bodyParser.json());
app.use(cors());

// API ROUTES
const posts = require('./routes/api/posts');
const users = require('./routes/api/users');
const user = require('./routes/api/user');

app.use('/api/posts', posts);
app.use('/api/users', users);
app.use('/api/user', user);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server is running on port ${port}`))