// Checks if a valid token is available before accessing to certain routes
// creates a userData object is so
const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        // remove bearer string form authorization token
        console.log(token)
        const decoded = jwt.verify(req.body.token, 'privateKey')
        req.userData = decoded;
    } catch (error) {
        return res.status(401).json({
            message: 'Auth failed'
        });
    }
    next();

};