import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home'
import Posts from './views/Posts'
import Register from './views/Register'
import Login from './views/Login'
import User from './views/User'
import Users from './views/Users'

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            name: 'Home',
            component: Home,
        },
        {
            path: '/posts',
            name: 'Posts',
            component: Posts,
        },
        {
            path: '/register',
            name: 'Register',
            component: Register,
        }, 
        {
            path: '/user',
            name: 'user',
            component: User,
        }, 
        {
            path: '/users',
            name: 'users',
            component: Users,
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
        },
    ],
    mode: 'history',
})