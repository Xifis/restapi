import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './assets/sass/main.scss'
import VueLocalStorage from 'vue-localstorage'
import VueJWT from 'vuejs-jwt'
import {
    BootstrapVue,
    IconsPlugin
} from 'bootstrap-vue'

new Vue({
    el: '#app',
    render: h => h(App),
    router,
})

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.use(VueJWT)
Vue.use(VueLocalStorage)
// Or you can specify any other name and use it via this.$ls, this.$whatEverYouWant
Vue.use(VueLocalStorage, {
    name: 'ls',
    bind: true //created computed members from your variable declarations
})